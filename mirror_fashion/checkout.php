<!--pure css - http://purecss.io/-->
<!--semantic ui - http://semantic-ui.com/-->

<!--Font Icons:-->
<!--icomoon.io-->



<!DOCTYPE html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap-flatly.css">
    <style type="text/css">
        input:invalid {
            border-color: red;
        }
    </style>
    
        
</head>

<body>

    <div class="jumbotron">
        <div class="container">
            <h1>Ótima escolha!</h1>
            <p>Obrigado por comprar na Mirror Fashion!
                Preencha seus dados para efetivar a compra.</p>
        </div>
    </div>

    <div class="container">
        <div class="panel panel-default">

            <div class="panel-heading" >
                <h2 class="panel-title">Sua compra</h2>
            </div>

            <div class="panel-body">
                <dl>
                    <dt>Produto</dt>
                    <dd><?= $_POST['nome'] ?></dd>

                    <dt>Cor</dt>
                    <dd><?= $_POST['cor'] ?></dd>

                    <dt>Tamanho</dt>
                    <dd><?= $_POST['tamanho'] ?></dd>

                    <dt>Preco</dt>
                    <dd><?= $_POST['preco'] ?></dd>
                </dl>
            </div>


        </div>

        <form>
            <fieldset>
                <legend>Dados Pessoais</legend>

                <div class="form-group">
                    <label for="nome">Nome completo</label>
                    <input type="text" class="form-control" name="nome" id="nome" autofocus required>

                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="email" class="form-control" name="email" id="email" >
                    </div>

                </div>

                <div class="form-group">
                    <label for="cpf">Cpf</label>
                    <input type="text" class="form-control" name="cpf" id="cpf"
                           data-mask="999.999.999-99"
                           required
                           placeholder="000.000.000-00"
                           pattern="[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}">

                </div>

                <div class="checkbox">
                    <input type="checkbox" value="sim" name="spam" id="spam" checked>
                    <label for="spam">
                        Quero receber spam da Mirror Fashion
                    </label>
                </div>

            </fieldset>

            <fieldset>
                <legend>Cartão de crédito</legend>

                <div class="form-group">
                    <label for="numero-cartao">Numero - CVV</label>
                    <input type="text" class="form-control" name="numero-cartao" id="numero-cartao">

                </div>

                <div class="form-group">
                    <label for="bandeira-cartao">Bandeira</label>
                    <select name="bandeira-cartao" id="bandeira-cartao" class="form-control">
                        <option value="master">MasterCard</option>
                        <option value="visa">VISA</option>
                        <option value="amex">American Express</option>
                    </select>

                </div>

                <div class="form-group">
                    <label for="validade-cartao">Validade</label>
                    <input type="month" class="form-control" name="validade-cartao" id="validade-cartao">

                </div>


            </fieldset>

            <button type="submit" class="btn btn-primary btn-lg pull-right">
                <span class="glyphicon glyphicon-thumbs-up  "></span>
                Confirmar Pedido
            </button>


        </form>

    </div>
    
    <script src="js/jquery.js"></script>
    <script src="js/inputmask-plugin.js"></script>
    <script type="text/javascript">
        email = document.querySelector('input[type=email]');
        
        email.oninvalid = function(){
            this.setCustomVaidity="";
            if(!this.validity.valid){
                this.setCustomValidity('Email inválido');
            }
        }
    </script>
    
</body>
</html>
