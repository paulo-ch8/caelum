
<!-- luiz.real@caelum.com.br -->

<!-- lea.verou.me/humble-border-radius -->
<!-- lea.verou.me/css3patterns -->
<!-- caniuse.com -->
<!-- prefixfree.js  - Ativa todos os prefixos para suas regras css -->
<!-- Graceful Degradation (metodologia ultrapassada) -> Progressive Enhancement (metodologia atual) -->
<!-- bit.ly/1uaAb0Q - Palestra do Luiz sobre Progressive Enhancement -->
<!-- CSS Transitions --> 
<!-- CSS Animations - Frames -->
<!-- CSS Transforms -->
<!-- css3maker.com -->
<!-- stylie - Ferramenta gráfica para gerar animaćões -->
<!-- 
    Reseters de CSS:
    Eric Meyer
    H5BP
    YUI reset
    Normalize
-->


<!--
    jquery - 80k
    mootools
    dojo
    Prototype
    zepto - mobile 5k
    polyfill.io - garantir compatibilidade. tamanho variável pela necessidade do navegador cliente
-->

<!DOCTYPE html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/reset.css">
        <!-- 
            Reseters de CSS:
            Eric Meyer
            H5BP
            YUI reset
            Normalize
        -->
        <link rel="stylesheet" href="css/estilo.css">
        <link rel="stylesheet" href="css/mobile.css" media="(max-width: 939px)">
        
    </head>
    
    <body>
        
        <header class="container">
            <h1><img src="img/logo.png" alt="Mirror Fashion"></h1>
            <div class="sacola">
                Nenhum item
            </div>
            <nav class="menu-opcoes">
                <ul>
                    <li><a href="#">Sua Conta</a></li>
                    <li><a href="#">Lista de Desejos</a></li>
                    <li><a href="#">Cartão Fidelidade</a></li>
                    <li><a href="#">Sobre</a></li>
                    <li><a href="#">Ajuda</a></li>
                </ul>
            </nav>
        </header>
        
        <section class="container destaque">
            
            <div class="busca">
                <h2>Busca</h2>
                <form action="http://google.com/search" name="searchForm">
                    <input type="search" name="q" id="q">
                    <input type="image" src="img/busca.png">
                </form>
            </div><!--fim .busca-->
            
            <div class="menu-departamentos">
                <h2>Departamentos</h2>
                <nav>
                    <ul>
                        <li>
                            <a href="#">Blusas e Camisas</a>
                            <ul>
                                <li><a href="#">Manga curta</a></li>
                                <li><a href="#">Manga comprida</a></li>
                                <li><a href="#">Camisa social</a></li>
                                <li><a href="#">Camisa casual</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Calćas</a></li>
                        <li><a href="#">Saias</a></li>
                        <li><a href="#">Vestidos</a></li>
                        <li><a href="#">Sapatos</a></li>
                        <li><a href="#">Bolsas e Carteiras</a></li>
                        <li><a href="#">Acessórios</a></li>
                    </ul>
                </nav>
            </div><!-- fim .menu-departamentos -->
        
            <img id="banner" src="img/destaque-home.png" alt="Promoćão: Big City Night">
        
        </section><!--fim .conteiner .destaque-->
        
        <div class="container paineis">
            
            <section class="novidades painel">
                
                <h2>Novidades</h2>
                <ul>
    <?php
    
        $conexao = mysqli_connect('localhost', 'root', '', 'WD43');
        $dados = mysqli_query($conexao, 'SELECT * FROM produtos LIMIT 0,13 ');
        $novidades = mysqli_fetch_array($dados);

        while ($produto = mysqli_fetch_array($dados)) : 
        
    ?>
                    <li>
                        <a href="produto.php?id=<?= $produto['id'] ?>">
                            <figure>
                                <img src="img/produtos/miniatura<?= $produto['id'] ?>.png" 
                                     alt="<?= $produto['nome'] ?>">
                                <figcaption>
                                    <?= $produto['nome'] ?>
                                    <strong>
                                        <?= $produto['preco'] ?>
                                    </strong>
                                </figcaption>
                            </figure>
                        </a>
                    </li>
    <?php 
    
        endwhile; 
    
    ?>
                </ul>
                
                <button type="button">Mosta mais</button>
                
            </section>
            
            <section class="mais-vendidos painel">
                
                <h2>Mais vendidos</h2>
                <ul>
    <?php
    
        $conexao = mysqli_connect('localhost', 'root', '', 'WD43');
        $dados = mysqli_query($conexao, 'SELECT * FROM produtos ORDER BY data DESC LIMIT 0,13 ');
        $novidades = mysqli_fetch_array($dados);

        while ($produto = mysqli_fetch_array($dados)) : 
        
    ?>
                    <li>
                        <a href="produto.php?id=<?= $produto['id'] ?>">
                            <figure>
                                <img src="img/produtos/miniatura<?= $produto['id'] ?>.png" 
                                     alt="<?= $produto['nome'] ?>">
                                <figcaption>
                                    <?= $produto['nome'] ?>
                                    <strong>
                                        <?= $produto['preco'] ?>
                                    </strong>
                                </figcaption>
                            </figure>
                        </a>
                    </li>
    <?php 
    
        endwhile; 
    
    ?>
                </ul>
                
                <button type="button">Mosta mais</button>
                
            </section><!--
            
     --></div>
        
        <footer>
            
            <h1><img src="img/logo-rodape.png" alt="Mirror Fashion"></h1>
            <ul class="redes-sociais">
                <li>
                    <a href="twitter" class="twitter">Twitter</a>
                </li>
                <li>
                    <a href="http://facebook.com/mf">Facebook</a>
                </li>
                <li>
                    <a href="googleplus">Google Plus</a>
                </li>
                   
            </ul>
            
        </footer>
        
        <script src="js/jquery.js"></script>
        <script src="js/validacao.js"></script>
        
    </body>
</html>
