window.onload = (function(){
    
    // querySelector Não é suportado por todos os navegadores
    var form = document.querySelector('form');
    var input = document.querySelector('[name=q]');
    
    form.onsubmit = function() {
        
        if(input.value == ''){
            input.style.backgroundColor = 'red';
            return false;
        }
        
    }
    
    input.onfocus = function(){
        input.style.backgroundColor = 'white';// Não é suportado por todos os navegadores
    }
    
    var banner = document.querySelector('#banner');
    var imagens = ['img/destaque-home.png', 'img/destaque-home-2.png' ];
    var atual = 0;
    
    function trocaImagem(){
        atual = (atual +1) % imagens.length;
        banner.src = imagens[atual];
    }
    
    setInterval(trocaImagem, 4000);
    
});

$('.painel').addClass('painel-compacto');

$('.painel button').click(function(){
    $(this).parent('.painel').removeClass('painel-compacto');
});