<?php

    $conexao = mysqli_connect('localhost', 'root', '', 'WD43');
    $dados = mysqli_query($conexao, 'SELECT * FROM produtos where id = '. $_GET['id']);
    $produto = mysqli_fetch_array($dados);

?>


<!DOCTYPE html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $produto['nome'] ?> produto da Mirror Fashion</title>
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/estilo.css">
        <link rel="stylesheet" href="css/mobile.css" media="(max-width: 939px)">
        <link rel="stylesheet" href="css/produto.css">
    </head>
    
    <body>
        <?php include('cabecalho.php'); ?>
        
        <div class="produto-back">
            <div class="container">
                <div class="produto ">

                    <h1><?= $produto['nome'] ?></h1>
                    <p>por apenas <?= $produto['preco'] ?></p>

                    <form action="checkout.php" method="POST">

                        <fieldset class="cores">
                            <legend>Escolha a cor:</legend>

                            <input type="radio" name="cor" id="verde" value="verde" checked>
                            <label for="verde">
                                <img src="img/produtos/foto<?= $produto['id'] ?>-verde.png" alt="Foto do produto verde">
                            </label>

                            <input type="radio" name="cor" id="rosa" value="rosa">
                            <label for="rosa">
                                <img src="img/produtos/foto<?= $produto['id'] ?>-rosa.png" alt="Foto do produto rosa">
                            </label>

                            <input type="radio" name="cor" id="azul" value="azul">
                            <label for="azul">
                                <img src="img/produtos/foto<?= $produto['id'] ?>-azul.png" alt="Foto do produto azul">
                            </label>

                        </fieldset>

                        <fieldset class="tamanhos">
                            <legend>Escolha o tamanho:</legend>
                            <input type="range" min="36" max="46" value="42" step="2" 
                                   name="tamanho" id="tamanho">
                            <output for="tamanho">42</output>
                        </fieldset>
                        
                        <input type="hidden" name="id" value="<?= $produto['id'] ?>" >
                        <input type="hidden" name="nome" value="<?= $produto['nome'] ?>" >
                        <input type="hidden" name="preco" value="<?= $produto['preco'] ?>" >

                        <input type="submit" class="comprar" value="Comprar">

                    </form>

                </div>

                <div class="detalhes">

                    <h2>Detalhes do produtos: </h2>
                    <p><?= $produto['descricao'] ?></p>

                    <table>
                        <thead>
                            <tr>
                                <th>Característica</th>
                                <th>Detalhe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Modelo</td>
                                <td>Cardigã 7845</td>
                            </tr>
                            <tr>
                                <td>Material</td>
                                <td>Algodão e Poliester</td>
                            </tr>
                            <tr>
                                <td>Cores</td>
                                <td>Azul, Rosa e Verde</td>
                            </tr>
                            <tr>
                                <td>Lavagem</td>
                                <td>Lavar a mão</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        
        <?php include('rodape.php'); ?>
        
        <script src="js/jquery.js"></script>
        <script type="text/javascript">
            $('#tamanho').on('input', function(){
                $('[for=tamanho]').val(this.value);
            });
        </script>
        
    </body>
</html>