function numberParaReal(numero) {
    var formatado = 'R$ ' + numero.toFixed(2).replace('.', ',');
    return formatado;
}

function realParaNumber(texto) {
//    var compativelComParseFloat = texto.replace('R$ ', '');
//    compativelComParseFloat.replace(compativelComParseFloat);
//    var valor = parseFloat(compativelComParseFloat);
    
    valor = parseFloat(texto.replace('R$ ', '').replace(',','.'));
    return valor;
}