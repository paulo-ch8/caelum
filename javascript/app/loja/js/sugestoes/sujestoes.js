// IIFE funcão imediatamente invocada
(function($){
    "use strict";
    
    $("input[type=button]").click(function(event){

        var inputSugestao = $("input[type=text]");
        var sugestao = inputSugestao.val();

        var existe = $(".sugestao").filter(function(){
            return $(this).text().toLowerCase() === sugestao.toLowerCase();
        });

        if(existe.length){
            var li = existe.closest("li");
            var votos = li.data("votos") + 1;
            li.data("votos", votos);
            li.find(".votos").text(votos+" votos"); 
            console.log(votos);
        }
        else{
            var nomeSugestao = $("<span>").addClass("sugestao").text(sugestao);
            var votos = $("<span>").addClass("votos").text("1 voto");

            $("<li>").data("votos", 1).append(nomeSugestao).append(votos).appendTo(".sugestoes");
            inputSugestao.val("").focus();
        }

    });

    $(".sugestoes").on("dblclick", "li", function(event){
        $(this).remove();
    });
    
    $("input[type=text]").autocomplete({
        source: function(entrada, resposta){
            var resultados = [];
            var novaSugestao = new RegExp(entrada.term, "i");
            
            $(".sugestao").each(function(){
                var sugestaoExistente = $(this).text();
                if(sugestaoExistente.match(novaSugestao)) {
                    resultados.push(sugestaoExistente);
                }
            });
            
            resposta(resultados);
        }
    })
    
})(jQuery);