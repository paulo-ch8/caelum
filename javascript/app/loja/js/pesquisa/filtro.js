(function($){
    "use strict";
    
    var input = $(".filtro input");

    input.keyup(function(){

        var busca = input.val().trim();
        var lis = $("#carrinho li");

        if(busca.length){
            lis.hide().find("h2, span").filter(function(){
                var valor = $(this).text();
                var regex = new RegExp(busca, "i");
                return regex.test(valor);
            }).closest("li").show();
        }
        else {
            lis.show();
        }

    });
})(jQuery);