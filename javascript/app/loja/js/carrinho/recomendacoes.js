// IIFE funcão imediatamente invocada
(function($, f){
    "use strict";
    
    $.getJSON("http://mirrorfashion.caelum.com.br/produtos?callback=?", 
        function(retorno){
            $(document).trigger("novasRecomendacoes", retorno);
        }
    );

    $(document).on("novasRecomendacoes", function(event, retorno){

        var painel = $("#recomendacoes");
        var ul = $("<ul>");

        $.each(retorno.produtos, function(){
            var li = $("<li>");
            var img = $("<img>").attr("src", this.imagem);
            var pNome = $("<p>").text(this.nome);
            var pPreco = $("<p>").text(f.numberParaReal(this.preco));

            li.append(img).append(pNome).append(pPreco).appendTo(ul);
        });

    //    Uma maneira:
    //    $("#recomendacoes").find("ul").remove().append(ul);

    //    Outra maneira:
        $("ul", painel).remove();

        ul.appendTo(painel);

    });

    $(document).one("novasRecomendacoes", function(event, retorno) {
        $("<a>").text("Novas Recomendacoes").addClass("aviso-recomendacao")
                .attr("href", "#recomendacoes").insertAfter(".aviso");
    });
})(jQuery, formatadorMoeda);